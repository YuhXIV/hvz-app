import React from 'react';
import './App.scss';
import ReactBootNavbar from './containers/Header/ReactBootNavbar/ReactBootNavbar.js';


export default class App extends React.Component {
  render() {
    return (
      <React.Fragment>
          <ReactBootNavbar />
          {this.props.children}
      </React.Fragment> 
    );
  }
}

