import React from 'react';
import Axios from 'axios';
import SquadsComponent from './../../../components/contents/Player/SquadsComponent';
import * as Constants from './../../../constants/constants';
import Button from '@material-ui/core/Button';
import './Gamepage.scss';

var api_url;

if (Constants.useLocalHost) {
    api_url = Constants.LocalHost;
} else {
    api_url = Constants.APIURL;
}

export default class Gamepage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isSignIn: false,
            game: null,
            registered: false,
        }
    }

    componentDidMount() {
        this.setState({
            isSignIn: true,
            game: Constants.DummyGameInfo,
            registered: Constants.DummyGameInfo.registered,
        })
        //this.authenticated();
        //this.getGame(); // what we need for real!
    }

    authenticated = () => {
        let config = {
            headers: {
                Authorization: 'Bearer ' + sessionStorage.getItem('token'),
            }
        }

        let that = this;
        Axios.get(api_url + "user/authenticated", config)
            .then(function (resp) {
                if (resp.data.status === 200) {
                    that.setState({ isSignIn: true })
                } else {

                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    getGame = () => {
        let config = {
            headers: {
                Authorization: 'Bearer ' + sessionStorage.getItem('token'),
            }
        }

        let that = this;
        Axios.get(api_url + "game/" + this.props.match.params.id, config)
            .then(function (resp) {
                that.setState({
                    game: resp.data,
                    registered: resp.data.registered,
                })
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    register = () => {
        let config = {
            headers: {
                Authorization: 'Bearer ' + sessionStorage.getItem('token'),
            }
        }

        let data = {
            GameId: this.state.id,
            IsAlive: true,
            IsPatientZero: false,
        }

        let that = this;
        Axios.post(api_url + "game/Player", data, config)
            .then(function (resp) {
                if (resp.data.status === 201) {
                    that.setState({
                        registered: true,
                    })
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        let gameContent = null;
        let squads = null;
        let mission = null;
        let squadtitle = null;

        if (this.state.game !== null) {
            if (this.state.registered) {
                gameContent = (
                    <React.Fragment>
                        <h2>{this.state.game.Title}</h2>
                        <b>Status: </b> {this.state.game.State} <br />
                        <b>Number of players: </b> {this.state.game.NumberOfPlayers} <br />
                        <b>Start: </b> {this.state.game.StartTime} <br />
                        <b>End: </b> {this.state.game.EndTime} <br />
                    </React.Fragment>
                )
                squadtitle = <h2>Squads</h2>
                if (this.state.game !== null && this.state.game.squads.length > 0) {
                    squads = this.state.game.squads.map(squad => (
                        <SquadsComponent
                            key={squad.id}
                            {...squad}
                        />
                    ));
                }
            } else {
                gameContent = (
                    <React.Fragment>
                        <h2>{this.state.game.Title}</h2>
                        <b>Status: </b> {this.state.game.State} <br />
                        <b>Number of players: </b> {this.state.game.NumberOfPlayers} <br />
                        <b>Start: </b> {this.state.game.StartTime} <br />
                        <b>End: </b> {this.state.game.EndTime} <br />
                        <Button onClick={this.register} variant="contained" color="primary" className="register-game-button">
                            Join game
                        </Button>
                    </React.Fragment>
                )
            }


            mission = (
                <React.Fragment>
                    <h3>{this.state.game.mission.name}</h3>
                    <b>Start: </b> {this.state.game.mission.start} <br />
                    <b>End: </b> {this.state.game.mission.end} <br />
                    <b>Mission description: </b> <br /> {this.state.game.mission.description}
                </React.Fragment>
            )
        }

        return (
            <React.Fragment>
                <div className='container'>
                    <div className="row">
                        <div className="col-8">
                            {gameContent}
                        </div>
                        <div className="col-4">
                            <h2>Mission: </h2>
                            {mission}
                        </div>
                    </div>
                    <div className="row squad-content">
                        <div className="col-8">
                            {squadtitle}
                            {squads}
                        </div>
                        <div className="col-4">
                            <h2>Chat</h2>
                            <div style={{ width: "100%", height: "100%", border: "solid", borderRadius: "5px", padding: "1rem", backgroundColor: "whitesmoke" }}>
                                <b>Chat is currently in developement</b>
                            </div>

                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}