import React from 'react';
import Axios from 'axios';
import './Player.scss';

import KillsComponent from './../../../components/contents/Player/KillsComponent';
import PlayerComponent from './../../../components/contents/Player/PlayerComponent';
import SquadsComponent from './../../../components/contents/Player/SquadsComponent';
import * as Constants from './../../../constants/constants';

var api_url;

if (Constants.useLocalHost) {
    api_url = Constants.LocalHost;
} else {
    api_url = Constants.APIURL;
}

export default class Player extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: { id: 1, username: "husUsername" }, //props.user
            player: null,
        }
    }

    componentDidMount() {
        this.setState({
            player: Constants.DummyPlayer
        })
        // this.getInfo(); // what we need for real!
    }

    getInfo = () => {
        let that = this;
        Axios.get(api_url + "player/" + this.props.match.params.id)
            .then(function (playerResp) {
                that.setState({ player: playerResp.data })
            })
            .catch(function (error) {
                console.log(error);
            });

        // Axios.get(api_url + "kill", { id: this.state.user.id })
        //     .then(function (killsResp) {
        //         that.setState({ kills: killsResp.data })
        //     }).catch(function (error) {
        //         console.log(error);
        //     })

        // Axios.get(api_url + "squad", { id: this.state.user.id })
        //     .then(function (squadResp) {
        //         that.setState({ squads: squadResp.data })
        //     }).catch(function (error) {
        //         console.log(error);
        //     })
    }

    render() {
        let kills = null;
        let squads = null;
        let player = null;

        if (this.state.player !== null && this.state.player.kills.length > 0) {
            kills = this.state.player.kills.map(kill => (
                <KillsComponent
                    key={kill.id}
                    {...kill}
                />
            ));
        }

        if (this.state.player !== null && this.state.player.squads.length > 0) {
            squads = this.state.player.squads.map(squad => (
                <SquadsComponent
                    key={squad.id}
                    {...squad}
                />
            ));
        }

        if (this.state.player !== null) {
            player = <PlayerComponent
                username={this.state.user.username}
                id={this.state.player.id}
                isAlive={this.state.player.isAlive}
                patientZero={this.state.player.patientZero}
                kills={this.state.player.numberOfKills}
            />
        }

        return (
            <React.Fragment>
                <div className="container">
                    <h1>Player</h1>
                    <div className="row">
                        <div className="col-8 player-info card">
                            <h2>Player info</h2>
                            {player}
                        </div>
                        <div className="col-4 kills card">
                            <h2>Kills</h2>
                            {kills}
                        </div>
                    </div>
                    <div className="row squad-content">
                        <h2>Squads</h2>
                        {squads}
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
