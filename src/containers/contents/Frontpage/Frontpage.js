import React from 'react';
import Axios from 'axios';
import GameComponent from './../../../components/contents/Game/GameComponent';
import './Frontpage.scss';
import * as Constants from './../../../constants/constants';

var api_url;

if (Constants.useLocalHost) {
    api_url = Constants.LocalHost;
} else {
    api_url = Constants.APIURL;
}

export default class Frontpage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isSignIn: false,
            registeredGame: [],
            games: [],
            finishedGame: [],
        }
    }

    componentDidMount() {
        this.setState({
            isSignIn: true,
            registeredGame: Constants.DummyGames.myGames,
            games: Constants.DummyGames.availableGames,
            finishedGame: Constants.DummyGames.myFinishedGames,
        })
        // this.authenticated();
        // this.getGames(); // what we need for real!
    }



    authenticated = () => {
        let config = {
            headers: {
                Authorization: 'Bearer ' + sessionStorage.getItem('token'),
            }
        }

        let that = this;
        Axios.get(api_url + "user/authenticated", config)
            .then(function (resp) {
                if (resp.data.status === 200) {
                    that.setState({ isSignIn: true })
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    getGames = () => {
        let config = {
            headers: {
                Authorization: 'Bearer ' + sessionStorage.getItem('token'),
            }
        }

        let that = this;
        Axios.get(api_url + "game/", config)
            .then(function (resp) {
                that.setState({
                    registeredGame: resp.data.registered,
                    games: resp.data.games,
                    finishedGame: resp.data.finished,
                })

            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        let registeredCard = null;
        let registeredTitle = null;
        let games = null;
        let finished = null;
        if (this.state.isSignIn === true) {
            if (this.state.registeredGame.length > 0) {
                registeredTitle = <h1 className="game-status">Registered</h1>
                registeredCard = (
                    this.state.registeredGame.map(game => (
                        <GameComponent
                            key={game.id}
                            {...game}
                            isSignedIn={this.state.isSignIn}
                        />
                    ))
                )
            }
        }

        games = this.state.games.map(game => (
            <GameComponent
                key={game.id}
                {...game}
                isSignedIn={this.state.isSignIn}
            />
        ))

        finished = this.state.finishedGame.map(game => (
            <GameComponent
                key={game.id}
                {...game}
                isSignedIn={this.state.isSignIn}
            />
        ))

        return (
            <React.Fragment>
                <div className="container">
                    <center><h1>Human VS Zombie</h1></center>
                    <div className="row main-page">
                        <div className="col-8 offset-2">
                            <p>
                                Welcome to the Human VS Zombie! This application digitalization
                                all the necessary information/documentation that an admin of the
                                game has to keep track of. This simplifies the process of an
                                admin of the game, to both get the game to run more smootly,
                                also saves the admin from lots of time and effort in running the game.
                            </p>
                            <p>
                                The application also has the userinterface for all players,
                                such that at anytime, player can go in and look at the status of the game
                                check up his teammates and saves the administrator from having
                                to gather everyone in a single meeting location. Instead
                                with a simple click, you get all the messages here on HvZ App.
                            </p>
                        </div>
                    </div>
                    {registeredTitle}
                    <div className="row registered-game-list">
                        {registeredCard}
                    </div>
                    <h1 className="game-status">Games</h1>
                    <div className="row game-list">
                        {games}
                    </div>
                    <h1 className="game-status">Finished</h1>
                    <div className="row finished-game">
                        {finished}
                    </div>
                </div>
            </React.Fragment>
        )
    }
}