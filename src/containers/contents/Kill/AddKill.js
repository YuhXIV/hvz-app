import React from 'react';
import AddKillFields from './../../../components/contents/Kill/AddKillComponent';
import Axios from 'axios';
import "./AddKill.scss";
import AddKillMap from './../../Map/AddKillMap/AddKillMap';
import * as Constants from './../../../constants/constants';
import { number } from 'prop-types';

var api_url;

if (Constants.useLocalHost) {
    api_url = Constants.LocalHost;
} else {
    api_url = Constants.APIURL;
}

export default class AddKill extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            killCode: false,
            lat: number,
            lng: number
        }
        this.getLatLng = this.getLatLng.bind(this);
    }

    createKill = (input) => {

        let headerConfig = {
            headers: {
                Authorization: `Bearer ${sessionStorage.getItem('token')}`
            }
        }
        let kill = {
            bitecode: input.bitecode,
            lat: this.state.lat,
            lng: this.state.lng
        }
        console.log("To axios:");
        console.log(kill);
        Axios.post(api_url + "/kill/", kill, headerConfig)
            .then((response) => {
                console.log(`API response ${response}`);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    getLatLng(latLng){
        this.setState({
            lat: latLng.lat,
            lng: latLng.lng
        })
        console.log("--Logging from AddKill.js--")
        console.log(latLng);
    }

    render() {
        let map = (<AddKillMap getLatLng={this.getLatLng} />);
        return (
            <React.Fragment>
                <div className="Login-sizing">
                    <AddKillFields killInfo={this.createKill} map={map} />                    
                </div>
            </React.Fragment>
        );
    }
}