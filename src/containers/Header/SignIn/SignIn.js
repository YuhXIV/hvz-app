import React from 'react';
import SignInFields from './../../../components/Header/SignIn/SignInComponent';
import Axios from 'axios';
import { Redirect } from 'react-router-dom'
import './SignIn.scss'
import * as Constants from './../../../constants/constants';

var api_url;

if (Constants.useLocalHost) {
    api_url = Constants.LocalHost;
} else {
    api_url = Constants.APIURL;
}

export default class SignIn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
        }
    }

    authenticate = (input) => {
        let that = this;
        Axios.post(api_url + "/user/authenticate", input)
            .then(function (response) {
                sessionStorage.setItem("token", response.data.token)
                that.setState({ redirect: true })
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    render() {
        let signin = null;
        if (this.state.redirect) {
            signin = <Redirect to="/" />
        } else {
            signin = <SignInFields
                userInfo={this.authenticate}
            />
        }
        return (
            <div className="Login-sizing">
                {signin}
            </div>
        );
    }
}