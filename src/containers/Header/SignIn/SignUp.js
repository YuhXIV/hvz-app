import React from 'react';
import SignUpFields from './../../../components/Header/SignIn/SignUpComponent';
import Axios from 'axios';
import { NavLink } from 'react-router-dom';
import "./SignUp.scss";
import * as Constants from './../../../constants/constants';

var api_url;

if (Constants.useLocalHost) {
    api_url = Constants.LocalHost;
} else {
    api_url = Constants.APIURL;
}

export default class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            signedup: false,
        }
    }

    createUser = (input) => {
        let that = this;
        Axios.post(api_url + "/user/register", input)
            .then(function () {
                that.setState({ signedup: true });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        let signupRendering = null;
        if (!this.state.signedup) {
            signupRendering = (
                <div className="Login-sizing">
                    <SignUpFields userInfo={this.createUser} />
                </div>
            )
        } else {
            signupRendering = (
                <div className="Login-sizing">
                    <h1 className="title">Sign up successful</h1>
                    <NavLink to={{ pathname: '/signIn' }} className="MuiTypography-root MuiLink-root MuiLink-underlineHover MuiTypography-body2 MuiTypography-colorPrimary links">Go to Sign in?</NavLink>
                </div>
            );
        }
        return (
            <React.Fragment>
                {signupRendering}
            </React.Fragment>
        );
    }
}