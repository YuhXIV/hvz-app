import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { NavLink } from 'react-router-dom';
import * as Constants from './../../../constants/constants';


// var api_url;

// if (Constants.useLocalHost) {
//     api_url = Constants.LocalHost;
// } else {
//     api_url = Constants.APIURL;
// }

export default class ReactBootNavbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isSignIn: false,
            registeredGame: [],
            games: [],
            finishedGame: [],
        }
    }

    componentDidMount() {
        this.setState({
            isSignIn: true,
            registeredGame: Constants.DummyGames.myGames,
            games: Constants.DummyGames.availableGames,
            finishedGame: Constants.DummyGames.myFinishedGames,
        })
        //this.authenticated();
        // this.getInfo(); // what we need for real!
    }

    render() {

        //For dynamically rendering myGames into the navbar
        let myGames = this.state.registeredGame;
        let renderMyGames;
        if (myGames.length > 0) {
            renderMyGames = myGames.map((game) => {
                let link = `/game/${game.id}`
                return (<NavDropdown.Item as={NavLink} to={link} key={game.id}>{game.name}</NavDropdown.Item>)
            });
        } else {
            renderMyGames = (<div></div>);
        }
        let myFinishedGames = this.state.finishedGame;
        let renderMyFinishedGames;
        if (myFinishedGames.length > 0) {
            renderMyFinishedGames = myFinishedGames.map((game) => {
                let link = `/game/${game.id}`
                return (<NavDropdown.Item as={NavLink} to={link} key={game.id}>{game.name}</NavDropdown.Item>)
            });
        } else {
            renderMyFinishedGames = (<div></div>);
        }


        //
        return (
            <Navbar sticky="top" bg="dark" expand="lg" variant="dark">
                <Navbar.Brand href="https://experisacademy.no/">
                    <img
                        src="https://i.imgur.com/CMKHbzE.png"
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                        alt="Experis logo"
                    />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
                        <NavDropdown title="My Maps" id="basic-nav-dropdown">
                            <NavDropdown.Item as={NavLink} to="/map/kills">Kill Map</NavDropdown.Item>
                            <NavDropdown.Item as={NavLink} to="/map/checkin">CheckIn Map</NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title="My Games" id="basic-nav-dropdown">
                            {renderMyGames}
                            <NavDropdown.Divider />
                            {renderMyFinishedGames}
                        </NavDropdown>
                        <Nav.Link as={NavLink} to="/addKill" exact>Add Kill</Nav.Link>
                    </Nav>
                    <Nav className=" mr-sm-2" >
                        <Nav.Link as={NavLink} to="/signin">Sign In</Nav.Link>
                        <Nav.Link as={NavLink} to="/signup">Sign Up</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        )
    }
}