import React from 'react';
import MyMap from '../MyMap/MyMap';
import './AddKillMap.scss';
import { Marker }from 'react-leaflet';

const killMapVar = {
    lat: 60.384,
    lng: 5.323,
    zoom: 16
  }

export default class AddKillMap extends React.Component{    
    constructor(props) {
        super(props);
        this.state = {
            latLng: [],
            currentPos: []
        }
        
    }
    
    componentWillMount(){
        console.log("Comp will mount! \n");
        this.setState({
            latLng: [killMapVar.lat, killMapVar.lng],
            currentPos: [killMapVar.lat, killMapVar.lng]
        })
        this.onMove = this.onMove.bind(this);
    }
    componentDidMount(){
        console.log("Comp did mount! \n");
    }
    onMove(e){
        console.log("onMoveCenter: ");
        console.log(e.getCenter());
        this.setState({
            currentPos: e.getCenter()
        })
        this.props.getLatLng(e.getCenter())
    }

    render(){
        let reactMarker = (
        <Marker position={this.state.currentPos} >          
        </Marker>);
       
        return(
            <div className="container .leaflet-AddKill-container">
                <MyMap hasOnMove={true} onMove={this.onMove} latlng={this.state.latLng} zoom={killMapVar.zoom} reactMarkers={reactMarker}></MyMap>
            </div>
        )
    }
}