import React from 'react';
import MyMap from '../MyMap/MyMap';
import './KillMap.scss';
import KillMarker from '../../../components/Map/Marker/KillMarker/KillMarker';

export default class KillMap extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
          lat: 60.384,
          lng: 5.323,
          zoom: 16,
          marker: [{
            key: 0,
            latlng: [60.38370, 5.32270],
            message: {
              info: "Killed in his own bed!",
              victimName: "Huy",
              timeOfDeath: null
            }
          },{
            key: 1,
            latlng: [60.38337, 5.32352],
            message: {
              info: "Slaughtered at spilde",
              victimName: "Markus",
              timeOfDeath: null
            }
        }]
      }
    }


    render(){
        let killMapVar = {
          lat: 60.384,
          lng: 5.323,
          zoom: 16,
          marker: [{
            key: 0,
            latlng: [60.38370, 5.32270],
            message: {
              info: "Killed in his own bed!",
              victimName: "Huy",
              timeOfDeath: ""
            }
          },{
            key: 1,
            latlng: [60.38337, 5.32352],
            message: {
              info: "Slaughtered at spilde",
              victimName: "Markus",
              timeOfDeath: ""
            }
          }]
        }

        let markers = killMapVar.marker;
        markers = markers.map((marker) => {
          let today = new Date();
          let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
          let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
          let dateTime = date+' '+time;
          marker.message.timeOfDeath = dateTime;
          console.log("current time: "+marker.message.timeOfDeath);
          return (marker);
        })
        killMapVar.marker = markers;

        const latlng = [killMapVar.lat, killMapVar.lng];

        let reactMarkers;
        if(markers.length > 0){
          reactMarkers = markers.map((mark) => {
              return (<KillMarker key={mark.id} marker={mark}></KillMarker>);
          });
        }else{
            reactMarkers = (<div></div>);
        }
        

        
        return(
            <div className="leaflet-container">
                <MyMap latlng={latlng} zoom={killMapVar.zoom} marker={markers} reactMarkers={reactMarkers}></MyMap>
            </div>
        )
    }
}