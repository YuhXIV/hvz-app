import React from 'react';
import { Map as LeafletMap, TileLayer }from 'react-leaflet';
import { number } from 'prop-types';
//import KillMarker from '../../../components/Map/Marker/KillMarker/KillMarker';

export default class MyMap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentMarkerPos: [],
      lat: number,
      lng: number,
      zoom: number
    }
    this.handleMove = this.handleMove.bind(this);
  }
  handleMove(e){
    console.log(this.state)
    console.log("Center: ");
    console.log(e.target.getCenter());
    console.log(this.props);
    if(e.target != null){
      console.log("not null");
      this.props.onMove(e.target);
    } 
  }
  handleClick(e){
    this.setState({ currentMarkerPos: e.latlng });
  }
  componentWillMount(){
    this.setState({
      currentMarkerPos: this.props.latlng
    })
  }



  componentDidMount(){
    console.log("Comp did mount! \n");
    
}

  render() {
    const latlng = this.props.latlng;
    console.log(this.props.hasOnMove)
    return (
      <LeafletMap 
        center={latlng} 
        zoom={this.props.zoom} 
        ref={(ref) => { this.map = ref; }} 
        onMove={this.props.hasOnMove ? this.handleMove : null}
        onClick={this.props.hasOnClick ? this.handleClick : null}
        >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'
        />
        {this.props.reactMarkers}
          
      </LeafletMap>
    );
  }
 }


