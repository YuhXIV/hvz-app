import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap.css';
import React from 'react';
import ReactDOM from 'react-dom';
//import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Route } from 'react-router-dom';
import Player from './containers/contents/Player/Player';
import SignIn from './containers/Header/SignIn/SignIn';
import SignUp from './containers/Header/SignIn/SignUp';
import KillMap from './containers/Map/KillMap/KillMap';
import FrontPage from './containers/contents/Frontpage/Frontpage'
import Gamepage from './containers/contents/Gamepage/Gamepage';
import CheckInMap from './containers/Map/CheckInMap/CheckInMap';
import AddKill from './containers/contents/Kill/AddKill';


ReactDOM.render((
    <BrowserRouter>
        <App>
            <Route exact path='/game/:id' component={Gamepage} />
            <Route exact path='/' component={FrontPage} />
            <Route exact path='/player/:id' component={Player} />
            <Route exact path='/map/checkin' component={CheckInMap} />
            <Route exact path='/map/kills' component={KillMap} />
            <Route exact path='/addKill' component={AddKill} />
            <Route exact path='/signin' component={SignIn} />
            <Route exact path='/signup' component={SignUp} />
        </App>
    </BrowserRouter>),
    document.getElementById('root'));
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
