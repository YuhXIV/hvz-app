import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';

import { NavLink } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        width: "40rem",
    },
    formControl: {
        margin: theme.spacing(1),
        width: "100%",
        marginTop: "2rem;",
    },
    button: {
        margin: theme.spacing(1),
        width: "100%",
        marginTop: "4rem;",
    },
    input: {
        display: 'none',
    },
    title: {
        justifyContent: "center",
        marginTop: "2rem",
    }
}));

const SignInFields = (props) => {
    const classes = useStyles();

    let username;
    let password;

    let submit = () => {
        props.userInfo({
            username: username,
            password: password
        });
    }

    let un = event => {
        username = event.target.value;
    }

    let pw = event => {
        password = event.target.value;
    }

    return (
        <React.Fragment>
            <div className={[classes.container, "container"].join(" ")}>
                <div className="offset-1 row col-10">
                    <div className="col-12">
                        <h1 className={classes.title}>Sign Up</h1>
                    </div>
                    <div className="col-12">
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="component-helper">Username</InputLabel>
                            <Input
                                type="text"
                                id="component-helper"
                                onChange={un}
                            />
                        </FormControl>
                    </div>
                    <div className="col-12">
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="component-helper">Password</InputLabel>
                            <Input
                                type='password'
                                onChange={pw}
                            />
                        </FormControl>
                    </div>
                    <div className="col-12">
                        <Button onClick={submit} variant="contained" color="primary" className={classes.button}>
                            Sign up
                        </Button>
                        <NavLink to={{ pathname: '/signin' }} className="MuiTypography-root MuiLink-root MuiLink-underlineHover MuiTypography-body2 MuiTypography-colorPrimary">Already have an account? Sign In</NavLink>
                    </div>
                </div>

            </div>
        </React.Fragment>

    )
}

export default SignInFields;