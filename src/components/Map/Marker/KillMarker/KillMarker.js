import React from 'react';
import { Marker, Popup }from 'react-leaflet';
import L from 'leaflet';



const KillMarker = (props) => {
    var killIcon = L.icon({
        iconUrl: 'https://i.imgur.com/T5Jfhxd.png',
    
        iconSize:     [15, 30], // size of the icon
        iconAnchor:   [0, 30],  // point of the icon which will correspond to marker's location
        popupAnchor:  [7.5, -30]// point from which the popup should open relative to the iconAnchor
        //shadowSize:   [50, 64], // size of the shadow
        //shadowAnchor: [4, 62],  // the same for the shadow
        
    });
    //position = [latitude, longitude] for bergen = [lat: 60.3865, lng: 5.3293]
    //mesage = string
    return(
        <Marker position={props.marker.latlng} icon={killIcon}>
            <Popup>
                <b>Name:</b> {props.marker.message.victimName} <br/>
                <b>Killed:</b> {props.marker.message.timeOfDeath} <br/>
                {props.marker.message.info}
            </Popup>
        </Marker>
    )  
}
export default KillMarker;