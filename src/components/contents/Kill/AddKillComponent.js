import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';

import { NavLink } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        width: "40rem",
    },
    formControl: {
        margin: theme.spacing(1),
        width: "100%",
        marginTop: "2rem;",
    },
    button: {
        margin: theme.spacing(1),
        width: "100%",
        marginTop: "4rem;",
    },
    input: {
        display: 'none',
    },
    title: {
        justifyContent: "center",
        marginTop: "2rem",
    }
}));

const AddKillFields = (props) => {
    const classes = useStyles();

    let bitecode;

    let submit = () => {
        props.killInfo({
            bitecode: bitecode
        });
    }

    let bc = event => {
        bitecode = event.target.value;
    }

    return (
        <React.Fragment>
            <div className={[classes.container, "container"].join(" ")}>
                <div className="offset-1 row col-10">
                    <div className="col-12">
                        <h1 className={classes.title}>Add Kill</h1>
                    </div>
                    <div className="col-12">
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="component-helper">Bite Code</InputLabel>
                            <Input
                                type="text"
                                id="component-helper"
                                onChange={bc}
                            />
                        </FormControl>
                    </div>
                    {props.map ? (<div className="col-12">{props.map}</div>) : ("")}
                    <div className="col-12">
                        <Button onClick={submit} variant="contained" color="primary" className={classes.button}>
                            Add Kill
                        </Button>
                        <NavLink to={{ pathname: '/' }} className="MuiTypography-root MuiLink-root MuiLink-underlineHover MuiTypography-body2 MuiTypography-colorPrimary">Don't want to add a kill? Go home!</NavLink>
                    </div>
                </div>
            </div>
        </React.Fragment>

    )
}

export default AddKillFields;