import React from 'react';
import './PlayerComponent.scss';

import { NavLink } from 'react-router-dom'

const PlayerComponent = (props) => {
    let playerInfo = null;

    if (props.isAlive) {
        playerInfo = (
            <div className="player-detail">
                <div className="row">
                    <div className="col-4">
                        <b>Username: </b>
                    </div>
                    <div className="col-8">
                        {props.username}
                    </div>
                </div>
                <div className="row">
                    <div className="col-4">
                        <b>Faction: </b>
                    </div>
                    <div className="col-8">
                        Human
                    </div>
                </div>
            </div>
        )
    } else {
        playerInfo = (
            <div className="player-detail">
                <div className="row">
                    <div className="col-4">
                        <b>Username: </b>
                    </div>
                    <div className="col-8">
                        {props.username}
                    </div>
                </div>
                <div className="row">
                    <div className="col-4">
                        <b>Faction: </b>
                    </div>
                    <div className="col-8">
                        Zombie
                    </div>
                </div>
                <div className="row">
                    <div className="col-4">
                        <b>Kills: </b>
                    </div>
                    <div className="col-8">
                        <NavLink to={{ pathname: '/kill/' + props.id }}> {props.kills} </NavLink>
                    </div>
                </div>
            </div>
        )
    }



    return (
        <React.Fragment>
            {playerInfo}
        </React.Fragment>
    )
}

export default PlayerComponent;