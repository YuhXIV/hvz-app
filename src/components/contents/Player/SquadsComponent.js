import React from 'react';
import './SquadsComponent.scss';
import { NavLink } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
}));

const SquadsComponent = (props) => {
    const classes = useStyles();

    let squadsMember = props.squadMember.map(squadMember => (
        <div className="col-3">
            <NavLink to={"/player/" + squadMember.id}>{squadMember.name}</NavLink>
        </div>
    ));

    let squadLeader = props.squadMember.find(squadMember =>
        props.squadLeader === squadMember.id
    );

    let squads = (
        <div className={[classes.root, "squad-card"].join(" ")}>
            <ExpansionPanel>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <div className="row col-12">
                        <div className="col-6">
                            <b>Squad name: </b> {props.squadName}
                        </div>
                        <div className="col-6">
                            <b>Squad Leader: </b> <NavLink to={"/player/" + squadLeader.id}>{squadLeader.name}</NavLink>
                        </div>
                    </div>

                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className="row col-12">
                        <div className="col-3">
                            <b>Squad members: </b>
                        </div>
                        <div className="col-9">
                            <div className="row">
                                {squadsMember}
                            </div>
                        </div>
                    </div>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        </div>
    )

    return (
        <React.Fragment>
            {squads}
        </React.Fragment>

    )
}

export default SquadsComponent;