import React from 'react';
import { NavLink } from 'react-router-dom';


const KillsComponent = (props) => {

    let kill = (
        <div className="row">
            <div className="col-6">
                <b>Name: </b> <p><NavLink to={"/player/" + props.victimId}>{props.victimName}</NavLink></p>
            </div>
            <div className="col-6">
                <b>Time of death: </b><p>{props.timeOfDeath}</p>
            </div>
        </div>
    )


    return (
        <React.Fragment>
            {kill}
        </React.Fragment>
    )
}

export default KillsComponent;