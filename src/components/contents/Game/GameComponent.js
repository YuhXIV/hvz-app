import React from 'react';
import { NavLink } from 'react-router-dom';


const GameComponent = (props) => {
    let game = null;
    if (props.isSignedIn) {
        game = (
            <NavLink className="card col-12 game-link" to={"/game/" + props.id}>
                <div className="row">
                    <div className="col-6">
                        <b>Game name: </b> {props.name} <br />
                        <b>Number of Players: </b> {props.players}
                    </div>
                    <div className="col-6">
                        <b>Start: </b> {props.start} <br />
                        <b>End: </b> {props.end}
                    </div>
                </div>
            </NavLink>
        )
    } else {
        game = (
            <NavLink className="card col-12 game-link" to={"/signin"}>
                <div className="row">
                    <div className="col-6">
                        <b>Game name: </b> {props.name}
                    </div>
                    <div className="col-6">
                        test
                    </div>
                </div>
            </NavLink>
        )
    }


    return (
        <div className="col-12">
            {game}
        </div>
    )
}

export default GameComponent;