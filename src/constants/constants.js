

export const APIURL = 'https://hvzapi.azurewebsites.net/api';
export const LocalHost = "https://localhost:44348/api/";
export const useLocalHost = false;

export const DummyGames = {
    myGames: [
        {
            id: 1,
            name: "noroff",
            players: 20,
            start: "01-02-19",
            end: "02-02-19",
        },
        {
            id: 2,
            name: "Experis",
            players: 20,
            start: "01-02-19",
            end: "02-02-19",
        },
        {
            id: 3,
            name: "WoW race",
            players: 20,
            start: "01-02-19",
            end: "02-02-19",
        },
    ],
    availableGames: [
        {
            id: 4,
            name: "heeeelllo",
            players: 20,
            start: "01-02-19",
            end: "02-02-19",
        },
        {
            id: 5,
            name: "Minde",
            players: 20,
            start: "01-02-19",
            end: "02-02-19",
        },
        {
            id: 6,
            name: "Dominos",
            players: 20,
            start: "01-02-19",
            end: "02-02-19",
        },
    ],
    myFinishedGames: [
        {
            id: 7,
            name: "im done...",
            players: 20,
            start: "01-02-19",
            end: "02-02-19",
        },
        {
            id: 8,
            name: "me 2....",
            players: 20,
            start: "01-02-19",
            end: "02-02-19",
        },
        {
            id: 9,
            players: 20,
            name: "why tho?",
            start: "01-02-19",
            end: "02-02-19",
        },
    ]
}

export const DummyPlayer = {
    id: 1,
    isAlive: false,
    patientZero: false,
    numberOfKills: 2,
    squads: [
        {
            id: 1,
            squadName: "squadName",
            squadLeader: 1,
            squadMember: [
                {
                    id: 1,
                    name: "imthebest",
                },
                {
                    id: 2,
                    name: "badas",
                },
                {
                    id: 3,
                    name: "orjan",
                },
                {
                    id: 4,
                    name: "tore",
                },
                {
                    id: 5,
                    name: "Markus",
                },
                {
                    id: 6,
                    name: "Azi",
                },
                {
                    id: 7,
                    name: "Thomas",
                },
            ]
        },
        {
            id: 2,
            squadName: "daheoooo",
            squadLeader: 9,
            squadMember: [
                {
                    id: 9,
                    name: "YuhXIV",
                },
                {
                    id: 10,
                    name: "Mooskar",
                },
                {
                    id: 11,
                    name: "Haxer",
                },
                {
                    id: 12,
                    name: "Chute",
                },
            ]
        }
    ],
    kills: [
        {
            id: 1,
            victimId: 12,
            victimName: "superNoob",
            timeOfDeath: "01-03-18",
        },
        {
            id: 2,
            victimId: 60,
            victimName: "AintGonnaDieEarly",
            timeOfDeath: "04-03-18",
        },
        {
            id: 3,
            victimId: 19,
            victimName: "PropablyWinning",
            timeOfDeath: "06-03-18",
        },
        {
            id: 4,
            victimId: 15,
            victimName: "CantCatchMe",
            timeOfDeath: "02-03-18",
        },
    ]
}
export const DummyKills = [{
    key: 0,
    gameId: 1,
    latlng: [60.38370, 5.32270],
    message: {
        info: "Killed in his own bed!",
        kilerId: 1,
        killerName: "Huy",
        victimId: 1,
        victimName: "Huy",
        timeOfDeath: ""
    }
}, {
    key: 1,
    gameId: 1,
    latlng: [60.38337, 5.32352],
    message: {
        info: "Slaughtered at spilde",
        killerName: "Ørjan",
        kilerId: 3,
        victimName: "Markus",
        victimId: 2,
        timeOfDeath: ""
    }
}]

export const DummyGameInfo = {
    id: 1,
    State: "in process",
    Title: "noroff",
    StartTime: "12-02-19",
    EndTime: "12-03-19",
    NumberOfPlayers: 20,
    registered: true,
    mission: {
        id: 1,
        name: "Save private Ørjan",
        description: "Ørjan is caught behind the enemies lines. Find him and remove him from danger!",
        start: "12-12-12",
        end: "13-13-13",
    },
    squads: [
        {
            id: 1,
            squadName: "squadName",
            squadLeader: 1,
            squadMember: [
                {
                    id: 1,
                    name: "imthebest",
                },
                {
                    id: 2,
                    name: "badas",
                },
                {
                    id: 3,
                    name: "orjan",
                },
                {
                    id: 4,
                    name: "tore",
                },
                {
                    id: 5,
                    name: "Markus",
                },
                {
                    id: 6,
                    name: "Azi",
                },
                {
                    id: 7,
                    name: "Thomas",
                },
            ]
        },
        {
            id: 2,
            squadName: "daheoooo",
            squadLeader: 9,
            squadMember: [
                {
                    id: 9,
                    name: "YuhXIV",
                },
                {
                    id: 10,
                    name: "Mooskar",
                },
                {
                    id: 11,
                    name: "Haxer",
                },
                {
                    id: 12,
                    name: "Chute",
                },
            ]
        }
    ],
}